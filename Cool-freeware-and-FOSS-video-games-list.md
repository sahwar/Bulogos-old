### A curated list of cool freeware & FOSS video games ###

#### All-time personal favorite free video-games ####

Notable freeware indie video games for MS Windows:
* [Pekka Kana 2](http://pistegamez.net/game_pk2.html) (side-scrolling 2D platformer, with a Level Editor to boost; I made the 2nd (fixed) Bulgarian translation of PK2) ([WEB-FORUM for PK2 fans](http://pistegamez.proboards18.com/))
* Laxius Power 3 & II & I (hardcore plot-driven indie jRPG-style 2D RPG) (HELP: map1, map2, [[useful partial walkthrough by Grogor](http://grogor2.tripod.com/) ([archived backup copy](http://archive.is/7LEgq))
Links:
http://laxiuspower-shrine.goodforum.net/t462-laxius-power-iii-how-to-unlock
http://laxiuspower-shrine.goodforum.net/t362-laxius-power-1-to-3-download-link
https://download.cnet.com/Laxius-Power-III/3000-7536_4-10800957.html
(TO DO: ADD MORE DOWNLOAD LINKS FOR Laxius Power 3)
* [Apprentice I](http://www.herculeaneffort.com/index.php?page=apprentice1) & [Apprentice II](http://www.herculeaneffort.com/index.php?page=apprentice2) (2D point-and-click adventure/quest game)
* [Cave Story](https://www.cavestory.org/) (legendary cult-classic 2D action side-scrolling platformer, hardcore to beat!) (**TO DO:** Translate Cave Story into Bulgarian!!!)
* [Blobby Volley 2](https://sourceforge.net/projects/blobby/) (also available [online](http://blobby.sourceforge.net/data/bv2browser/index.html)!)
* ? (RTS)
* [Wormux](https://github.com/yeKcim/warmux) (Worms3-2D clone)
* Zack McKraken (freeware) & [Zak McKraken BTaS](http://www.mckracken.net/cms/directorscut.html)
* etc.

More:
caiman.us
https://alternativeto.net//list/479/free-games
https://alternativeto.net/software/caiman-us/
(to add more hyperlinks to freeware&abandonware video games)
