## Viable offline & online [payment-methods](https://en.wikipedia.org/wiki/Payment#Methods) & payment systems 2019 ##

### Ancient (older than feudalism) exchange methods ###
* Physical resource(s)/energy/food&drink/sex/tech/information-data barter
* Legal, under-the-table, and 'illegal' payment via providing of services/physical goods/tech/manual (and etc.) help/information-data
* Bidding in auctions (for goods)

### Classic (e-)banking ###
* In-country & International bank transfer (IBAN & SWIFT) (check bank tariffs/costs/charges)

### Online ###
* **PayPal.com** (works with linked debit cards and credit cards)
* **Patreon.com**
* **LiberaPay.com**
* **Flattr.com**
* Western Union (? Sorta reliable, mixed reviews.)
* MoneyGram (? Sorta reliable, mixed reviews.)
* bitcoin & other cryptocurrencies (???) (e.g. Etherium ETH, Litecoin, etc.) [I PERSONALLY **DO NOT** RECOMMEND CRYPTOCURRENCIES AS A VIABLE ALTERNATIVE TO REGULAR CURRENCIES DUE TO Ponzi-scheme-like CONCERNS OVER THE NATURE&AIMS OF CRYPTOCURRENCIES]

### Payment in cash ###
* In-person p2p cash-exchange
* Payment-on-delivery (наложен платеж) (geolocation-centric)
* Legal, under-the-table, and 'illegal' money payment
* Bidding in auctions (payment with money)
* Money payment in advance (per agreement via written legal contract)
* [Bulgarian Post](http://bgpost.bg/bg/109) payment (Bulgaria-centric)
* [ePay.bg](https://www.epay.bg/v3main/front?p=mrcs) (Bulgaria-centric)

### Etc. ###
* Consignation
* Re-selling of goods/services, or serving as proxy, or outsourcing (?)
* Dropshipping (?)
* Selling your items on Amazon.com sites, eBay.com sites, Etsy.com ([alternatives](https://www.finder.com/sites-like-etsy), [alternatives2](https://www.shoplo.com/blog/sites-like-etsy/))
* [Freelancing online](https://www.quora.com/Which-are-the-best-websites-to-hire-freelancers) - freelancer.com, upwork.com

### [Crowdfunding](https://www.crowdfunding.com/) (NOT so viable...) ###
_([information in Bulgarian](https://www.crowdfunding.com/), [more information](https://www.fundable.com/learn/resources/guides/crowdfunding/what-is-crowdfunding))_
* https://www.kickstarter.com/
* https://www.indiegogo.com/
* https://www.gofundme.com/
