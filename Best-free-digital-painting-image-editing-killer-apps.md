### 🎨 The Ultimate List of Digital Painting & Image Editing Apps ###
(Върховният списък със софтуер за дигитално рисуване и редактиране на изображения)

Ultimate list of image editing, graphics editing, digital drawing&painting software (the focus is on open-source/FOSS and freeware apps). These apps are for the modern desktop OS trinity: Microsoft Windows, GNU/Linux, Apple macOS. Most apps in the list are FOSS or freeware. The list aims at being highly inclusive while still focusing on recommending the best apps. Most of the digital painting apps in the list also support using a graphics tablet (e.g. Wacom tablets) aside from using a computer mouse and keyboard (check each app's features list or documentation for details).

This list is particularly suitable for people who add graphics-editing art-software to their Linux distro (or who package art software for related repositories , i.e. compiled binary-from-source packages) and for people who use computers mostly for digital/mixed-media art production/editing.

_THERE ARE STILL SOME APPS TO ADD TO THIS LIST SO DON'T CONSIDER IT TOO COMPLETE EVEN THOUGH IT **IS** AIMING AT BEING COMPREHENSIVE_


* [Adobe Photoshop](http://www.adobe.com/products/photoshop.html) [Adobe CreativeCloud](https://www.adobe.com/uk/creativecloud.html) (commercial, has a free trial version)
* [Alchemy](http://al.chemy.org/) OPEN-SOURCE
* [AliveColors](?) (commercial)
* [Artipic](?) (commercial)
* [Artweaver Free](https://www.artweaver.de/en/download/artweaverfree) FREEWARE _(RECOMMENDED)_
* [Artweaver Plus (newer, commercial)](https://www.artweaver.de/en) (commercial)
* [Aseprite](https://www.aseprite.org/) (the newer version is commercial, has a freeware trial version; has an on open-source fork of the last open-source version: [LibreSprite](https://github.com/LibreSprite/LibreSprite)) _(RECOMMENDED)_
* [Autodesk SketchBook](https://sketchbook.com/) [2](https://www.autodesk.co.uk/products/sketchbook/subscribe) (commercial, has a free trial version and a get-it-for-free programme for students/educators)
* [AzPainter](https://github.com/Symbian9/azpainter) [2](https://osdn.net/projects/azpainter/releases) [3](https://azworldweb.wordpress.com/), [4](https://azworldweb.wordpress.com/about/) (also by the same author, Azel: AzDrawing, AzPainterB; it's a lot like an open-source Paint Tool SAI but more lightweight and sorta different!) (**[AzPainter AppImage for Linux!](https://bintray.com/probono/AppImages/AzPainter#files)** by the famous [probono (5)](https://dl.bintray.com/probono/AppImages/)!), [(6)](https://www.opendesktop.org/p/1233213/), [(7) (Ubuntu PPA for AzPainter)](https://launchpad.net/~alex-p/+archive/ubuntu/azpainter), [8](https://appimage.github.io/AzPainter/), [(9)](https://askubuntu.com/questions/942802/how-do-i-install-azpainter-2-0-6), [(10)](https://www.deviantart.com/frankqbe/art/AzPainter-212-English-Version-Download-488404806), [(11)](https://www.deviantart.com/0xconfig/journal/AzPainter-Linux-2-x-painting-tutorial-727622415) (AzPaint art-drawing tutorial), [(12)](https://www.deviantart.com/tag/azpainterlinux), [(13)](http://cosbyart.blogspot.com/2018/04/art-review-azdrawing-and-azpainter.html) (reviews of AzDrawing and AzPainter), [(14)](http://cosbyart.blogspot.com/2018/04/art-review-azdrawing-and-azpainter.html), [(15)](https://hp.vector.co.jp/authors/VA033749/soft/azpainter2.html) (AzPainter for Windows98/XP/7_64-bit), [(16)](https://github.com/abcang/azpainter-mac) (AzPainter for macOS) [(AzDrawing)](https://osdn.net/projects/azdrawing/), [(AzMiniPainter)](https://osdn.net/projects/azminipainter/), [(AzPainterB)](https://osdn.net/projects/azpainterb/), [(AzPainter for FreeBSD)](https://www.freshports.org/graphics/azpainter), [(Azel's developer page](https://osdn.net/users/azelpg/) ([etc](https://senlinos.com/post/azpainter-2.1.2-and-my-git-warehouse/)) _(HIGHLY RECOMMENDED)_
<!--
http://archive.is/2BxWj
http://archive.is/FQES1
https://translate.google.bg/translate?sl=auto&tl=en&u=http%3A%2F%2Farchive.is%2FFQES1
https://www.youtube.com/watch?v=rwxCSGqJgQk
https://www.deviantart.com/frankqbe/art/AzPainter-212-English-Version-Download-488404806
http://azsky2.html.xdomain.jp/linux/azpainter.html
http://alexdukal.blogspot.com/2011/11/azdrawing-2-en-espanol.html
http://mundoazdrawing.blogspot.com/p/descarga.html
http://alexdukal.blogspot.com/2011/11/azdrawing-2-en-espanol.html
http://translate.google.com/translate?depth=2&hl=en&rurl=translate.google.com&sl=auto&sp=nmt4&tl=en&u=http://azsky2.html.xdomain.jp/linux/azdrawing.html
https://drive.google.com/file/d/1QQCySRJhyJRV_p01q3VyVJnmaykin4a3/view
https://www.dropbox.com/s/eon45dj3j92k6tj/AzDrawing_2.zip?dl=0
https://www.dropbox.com/s/e554l207dfqa5rg/lang_es.txt?dl=0
http://archive.is/vcB61
http://azsky2.html.xdomain.jp/
Azel: azelpg@gmail.com , azel@rinku.zaq.ne.jp
http://sai.detstwo.com/forum/index.php/topic,597.0.html
https://puppylinux.org/wikka/Azpainter
http://hp.vector.co.jp/authors/VA033749/format.html
http://hp.vector.co.jp/authors/VA033749/faq/azdw2.html
http://hp.vector.co.jp/authors/VA033749/faq/azpainter.html
http://hp.vector.co.jp/authors/VA033749/tips_azpt/index.html
http://hp.vector.co.jp/authors/VA033749/tips_azdw2/index.html
http://hp.vector.co.jp/authors/VA033749/tips_azdw2/anim.html
-->
* [BrashMonkey Sprite (!)](https://brashmonkey.com/download-spriter-beta/)
* [BrashMonkey Spriter](https://brashmonkey.com/download_spriter/?utm_campaign=free-traffic&utm_source=solutions-softonic-com&utm_medium=referral#) (Windows, Linux, macOS) _(RECOMMENDED)_
* [Chasys Draw IES](http://www.jpchacha.com/chasysdraw/) FREEWARE _(HIGHLY RECOMMENDED)_
* [ChibiPaint mod (oakaki)](https://github.com/thenickdude/chibipaint)
* [ChibiPaint Oekaki](http://www.chibipaint.com/)
* [ChickenPaint (HTML5 port of ChibiPaint oekaki web-image-board)](https://github.com/thenickdude/chickenpaint) [2](https://www.chickensmoothie.com/Forum/viewtopic.php?f=6&t=3014028) _(RECOMMENDED)_
* [CinePaint](http://www.cinepaint.org/more/) _(RECOMMENDED)_
* [CLIP STUDIO PAINT](https://www.clipstudio.net/en) (successor to Manga Studio (Comic Studio)) (commercial)
* [Corel Painter](https://www.painterartist.com/en/) (commercial)
* [Corel PaintShop Pro](https://www.paintshoppro.com/en/) (commercial)
* [CorelDRAW](https://www.coreldraw.com/en/) (commercial)
* [Cosmigo ProMotion NG](https://www.cosmigo.com/) [2](https://store.steampowered.com/app/671190/Pro_Motion_NG/) (commercial pixel-art editor, VERY CAPABLE) _(RECOMMENDED)_
* [Darktable](https://darktable.org/) (photo retouching & photo color adjustments) OPEN-SOURCE _(RECOMMENDED)_
* [Dia](https://wiki.gnome.org/Apps/Dia/) (diagram-drawing app) [2](http://dia-installer.de/) [3](https://sourceforge.net/projects/dia-installer/) OPEN-SOURCE _(RECOMMENDED)_
* [Dibuja](https://launchpad.net/dibuja) [2](https://www.opendesktop.org/s/Apps/p/1129305/) OPEN-SOURCE
* [DIGImend](http://digimend.github.io/) (NOT an image editor but FOSS Linux drivers for non-Wacom graphics-tablets) [2](https://github.com/DIGImend/digimend-kernel-drivers) [3](https://github.com/DIGImend)
* [Dotgrid](https://hundredrabbits.itch.io/dotgrid) [2 Online version of Dotgrid](https://hundredrabbits.github.io/Dotgrid/)
* [Dopey](https://github.com/dopeyanimation/dopey) (A 2D animation program based on MyPaint and an older fork of MyPaint)
* [dpixel](https://marcoc2.itch.io/dpixel) ([GitHub](https://github.com/marcoc2/dpixel)) OPEN-SOURCE _(RECOMMENDED)_
* [DragonBones Pro](?) (commercial)
* [DrawPile](https://github.com/drawpile/Drawpile) [2](http://drawpile.net/) _(HIGHLY RECOMMENDED)_
* [Drawing (a drawing app for Linux GNOME DE](https://github.com/maoschanz/drawing)
* [EasyPaintTool SAI (English warez)](http://detstwo.com/sai/) [official website 2](https://www.systemax.jp/en/sai/) [3](https://www.paint-tool-sai.at/filebase/index.php?file-list/) [4 deviantART](https://www.deviantart.com/vilebile/journal/Have-My-Paint-Tool-Sai-545675976) [5 MediaFire](http://www.mediafire.com/file/h7bfcoht16ulzia/sai+n+sh%2At.zip)[dll](https://www.mediafire.com/file/cfu05u9u8g1yc11/sfl.dll/file) [6](https://www.deviantart.com/dokkoni/art/Free-Paint-tool-Sai-B-637433421) [7](https://www.deviantart.com/sagemint/art/Floating-panels-in-Sai-settings-356651107) _(HIGHLY RECOMMENDED)_
* [EasyPaint](https://github.com/Gr1N/EasyPaint) [2](https://www.linux-apps.com/p/1131199/) _(RECOMMENDED)_
* [ExPhoto (sister project to Pixia)](http://www.ne.jp/asahi/mighty/knight/exphoto.html) _(RECOMMENDED)_
* [EZ Paint (freeware alternative to MS Paint for WinXP, for Windows7+)](http://ezpaintsoftware.com/EZdownloads.html) _(HIGHLY RECOMMENDED)_
* [FireAlpaca](https://firealpaca.com/) FREEWARE _(HIGHLY RECOMMENDED)_
* [FlamePainter](https://www.escapemotions.com/products/flamepainter/) (commercial)
* [GIMP Paint Studio](https://github.com/Draekko-RAND/gps-gimp-paint-studio)
* [GimPhoto (GIMP with tweaked GUI)](http://www.gimphoto.com/) [2](https://sourceforge.net/projects/gimphoto/) FREEWARE
* [GIMPshop](https://www.gimpshop.com/)
* [GIMP](https://www.gimp.org/) _(HIGHLY RECOMMENDED)_
* [gnome-paint](https://launchpad.net/gnome-paint) [2](deadlink?)
* [Goya (pixel-art editor)](https://github.com/jackschaedler/goya) ([online version](http://jackschaedler.github.io/goya/))
* [gpaint](https://www.gnu.org/software/gpaint/)
* [GraFX2](http://grafx2.chez.com/) [2](https://pulkomandy.tk/projects/GrafX2/downloads) _(HIGHLY RECOMMENDED)_
* [GraphicsGale](https://graphicsgale.com/us/download.html) FREEWARE as of 2018! _(HIGHLY RECOMMENDED)_
* [Harmony (by Ricardo Cabella)](http://mrdoob.com/projects/harmony/)
* [HermiRES (Commodore64 painting app)](http://www.nightfallcrew.com/18/03/2014/hermires-v1-29-c64-hires-bitmap-editor/) [2](http://www.nightfallcrew.com/10/02/2013/hermires-v1-26-c64-hires-bitmap-editor/)
* [Hornil StylePix Free](http://hornil.com/products/stylepix/) FREEWARE _(RECOMMENDED)_
* [iDraw3](http://www.rpgmaker.pl/?co=download&typ=programy&id=idraw) ([direct download](www.rpgmaker.pl/download/programy/idraw/idraw.zip) and [link2](http://tsukuru.pl/index.php?link=programy/iDraw)) [2](http://www.rpgmaker.org/downloads/view.asp?id=248) [3](https://www.purezc.net/forums/index.php?showtopic=56005) [4](https://www.deviantart.com/tag/idraw3) [4 (Wine)](https://appdb.winehq.org/objectManager.php?sClass=application&iId=3742) [5tutorial](http://archive.fo/8fupk) FREEWARE _(RECOMMENDED)_
* [ImagePlay](https://github.com/cpvrlab/ImagePlay) OPEN-SOURCE
* [Inkscape](https://inkscape.org/) (vector-images editor) OPEN-SOURCE _(HIGHLY RECOMMENDED)_
* [iPaint (based on Paintbrush above, macOS)](https://sourceforge.net/projects/ipaint/) [2](http://ipaint.sourceforge.net/), [3](http://macfun.svn.beanstalkapp.com/ipaint/)
* [JPixel (by emad.itch.io)](https://emad.itch.io/jpixel) _(HIGHLY RECOMMENDED)_
* [JS Paint (MS Paint for Windows98/XP remade using JavaScript!)](https://github.com/1j01/jspaint/) ([online](http://jspaint.app/))
* [JugiPaint](http://jugipaint.com/) (commercial) _(RECOMMENDED)_
* [JumpPaint by MediBang, official ShonenJump manga-drawing app!](https://medibangpaint.com/en/jumppaint/) [2](https://medibangpaint.com/en/app-download/#medibangpaint) + BRUSHES = https://medibangpaint.com/en/brush/ + MATERIALS = https://medibangpaint.com/en/material/ _(HIGHLY RECOMMENDED)_
* [KolourPaint](http://kolourpaint.sourceforge.net/) _(HIGHLY RECOMMENDED)_
* [Krita](https://krita.org/en/) [2](https://github.com/KDE/krita) _(HIGHLY RECOMMENDED)_
* [KXStitch](https://www.opendesktop.org/p/1126920/) (stitch-making sofware) OPEN-SOURCE
* [LazPaint](https://sourceforge.net/projects/lazpaint/) [2](https://github.com/bgrabitmap/lazpaint) [3](https://code.launchpad.net/lazpaint) [4](http://wiki.freepascal.org/LazPaint) _(HIGHLY RECOMMENDED)_
* [LightZone](http://lightzoneproject.org) [2](https://github.com/ktgw0316/LightZone)
* [Linux Wacom Project, The](https://linuxwacom.github.io/) (NOT an image editor but FOSS Linux drivers for Wacom graphics-tablets) [2](https://github.com/linuxwacom)
* [Livebrush AdobeAIR](deadlink?)
* [LoSpec (online pixel-art editor)](https://lospec.com/pixel-editor/app) [Pixel-art software list](https://lospec.com/pixel-art-software-list) + [Pixel-art color palettes](https://lospec.com/palette-list)
* [Lunapic Editor (online)](https://www190.lunapic.com/editor/) (online pixel-art editor)
* [MakePixelArt (online)](http://makepixelart.com/free/)
* [MediBang Paint Pro](https://medibangpaint.com/en/pc/) FREEWARE _(HIGHLY RECOMMENDED)_
* [Milton (by Sergio Gonzalez, open-source=MIT license) (!), comparable to SpeedyPainter but infinite-canvas VECTOR drawing](https://www.miltonpaint.com/) [2](https://github.com/serge-rgb/milton/releases) _(HIGHLY RECOMMENDED)_
* [miniPaint (online HTML5-based image editor)](https://github.com/viliusle/miniPaint) ([online version (press F11 key for fullscreen view!)](http://viliusle.github.io/miniPaint/))
* [Mischief Free v2.1.6](https://www.madewithmischief.com/downloads/) _(RECOMMENDED)_
* [Moai (by Ken Thornton), pixel-art editor](http://members.allegro.cc/sirocco/nav_mo.htm) v1.7 (for Windows), _FREEWARE_ _(RECOMMENDED)_
* [moPaint (by the same author as JS Paint](https://github.com/1j01/mopaint) ([online version](https://github.com/1j01/mopaint))
* [MQSprite](https://github.com/eigenbom/MQ-Sprite) OPEN-SOURCE (Windows)
* [MS Paint (for WindowsXP SP3)](https://archive.org/details/MSPaintWinXP) [Hidden advanced features (tips&tricks) of MS Paint for WinXP!!!](https://sahwar.wordpress.com/2014/09/20/the-secret-features-of-ms-paint/) (in Bulgarian and in English)
* [mtPaint](http://mtpaint.sourceforge.net/) [2](http://sourceforge.net/projects/mtpaint/files/mtpaint/) _(RECOMMENDED)_
* [MyBrushes (macOS)](?) (commercial)
* [MyPaint](http://mypaint.org/) OPEN-SOURCE _(HIGHLY RECOMMENDED)_
* [openCanvas](http://www.portalgraphics.net/en/oc/) (commercial)
* [OpenToonz + GTS (by anime-makers Studio Ghibli&DWANGO!)](https://opentoonz.github.io/e/) [2](https://github.com/opentoonz/dwango_opentoonz_plugins) [3](https://github.com/opentoonz/opentoonz) [4](https://github.com/opentoonz/GTS) _(HIGHLY RECOMMENDED)_
* [Paint of Persia](https://dunin.itch.io/ptop) (a pixel-art rotoscoping tool useful for making pixel-art sprites&animations) _FREEWARE_ (Windows, macOS, Linux) _RECOMMENDED_
* [paint.net (Windows7+ only)](https://www.getpaint.net/) FREEWARE (.NET - https://www.microsoft.com/en-us/download/details.aspx?id=36805 + https://www.microsoft.com/en-us/download/details.aspx?id=56115 ) _(HIGHLY RECOMMENDED)_
* [Paintbrush (macOS)](https://sourceforge.net/projects/paintbrush/)
* [PaintStar](https://sites.google.com/site/wangzhenzhou/home) FREEWARE _(RECOMMENDED)_
* [Papagayo (lipsync)](https://en.wikipedia.org/wiki/Papagayo_(software)]
* [Pencil2D](https://www.pencil2d.org/) [2](https://github.com/pencil2d/pencil) (2D hand-drawn animation editor) OPEN-SOURCE
* [Photivo](http://photivo.org/)
* [Photo Flow](?) OPEN-SOURCE
* [Photobie](http://www.tucows.com/preview/500316)
* [PhotoFlare](http://photoflare.io/contributing/translations/) [2](https://github.com/PhotoFlare/photoflare) OPEN-SOURCE_(RECOMMENDED)_
* [PhotoScape](http://photoscape.org/ps/main/index.php) FREEWARE
* [Phototonic (!)](https://www.linux-apps.com/p/1131097/) [2](https://github.com/pisilinux/playground)
* [Phoxo](http://www.phoxo.com/en/) FREEWARE
* [Pickle Editor](http://www.pickleeditor.com/)
* [PictBear](http://www.fenrir-inc.com/us/pictbear/) FREEWARE [2](https://www.softpedia.com/get/Multimedia/Graphic/Graphic-Editors/PictBear.shtml) _(RECOMMENDED)_
* [PikoPixel(macOS)](http://twilightedge.com/mac/pikopixel/) OPEN-SOURCE _(RECOMMENDED)_
* [Pinta](https://pinta-project.com/pintaproject/pinta/) _(RECOMMENDED)_
* [Piskel (online!)](https://www.piskelapp.com/) (online!!!) _(HIGHLY RECOMMENDED)_
* [Pixarra Pro Studio](http://www.pixarra.com/pro_studio.html) [2](http://www.pixarra.com/download.html), [Pixarra PixelStudio](http://www.pixarra.com/pixel_studio.html), [Pixarra Pro Studio](http://www.pixarra.com/paint_studio.html) (commercial)
* [pixeditor](https://github.com/z-uo/pixeditor) (alpha-version animated pixel-art editor)
* [Pixel Studio](https://itunes.apple.com/us/app/pixel-studio/id1446750012?mt=12) (macOS, commercial)
* [Pixel.Tools (open-source COLLABORATIVE online pixel-art editor)](https://prominentdetail.github.io/Pixel.Tools/) _(HIGHLY RECOMMENDED)_
* [Pixelitor (Java-based)](http://pixelitor.sourceforge.net/) [2](https://github.com/lbalazscs/Pixelitor)
* [Pixelmator Pro (macOS)](?) (commercial?)
* [Pixen (macOS)](https://pixenapp.com/) formerly OPEN-SOURCE, now commercial) _(HIGHLY RECOMMENDED)_
* [Pixia](http://www.ne.jp/asahi/mighty/knight/aboutpixia.html) FREEWARE [v6_32bit, v6_x64, v4_32bit] _(RECOMMENDED)_
* [PixieEngine (online)](https://pixieengine.com/)
* [Pixie](http://pixieengine.com/)
* [Pixilart (online)](https://www.pixilart.com/)
* [Pixlr (online)](https://pixlr.com/editor/)) (online pixel-art editor)
* [Pixly (for Android)](http://pixly.meltinglogic.com/)
* [Poxi (online flat pixel-art editor](https://github.com/maierfelix/poxi) _(RECOMMENDED)_
* [Project Dogwaffle Free v1.2](?) {paid commercial Project Dogwaffle is now called [Howler11](https://www.thebest3d.com/howler/)} [3](https://www.thebest3d.com/dogwaffle/penny/index.html) _(RECOMMENDED)_
* [PyxelEdit (commercial, has a freeweare beta version)](http://pyxeledit.com/about.php) _(RECOMMENDED)_
* [PyxleOS (!)](https://github.com/Dakkra/PyxleOS)
* [QAquarelle](https://sourceforge.net/projects/qaquarelle/)
* [rassam-paint](https://www.linux-apps.com/p/1129308/) _(RECOMMENDED)_
* [Rebelle (by EscapeMotions)](https://www.escapemotions.com/products/rebelle/) [2](https://my.smithmicro.com/rebelle-3-digital-paint-brush-program.html) (commercial)
* [S-Paint](https://github.com/Kulmetor43/S-Paint/blob/master/index.md) [2](https://kulmetor43.github.io/S-Paint/)
* [Scri.ch (online)](https://about.scri.ch/) ([about](https://about.scri.ch/))
* [Seashore (macOS)](https://github.com/robaho/seashore) [2](https://sourceforge.net/projects/seashore/)
* [sK1](https://sk1project.net/) [2](https://github.com/sk1project/sk1-wx/) _(RECOMMENDED)_
* [Skencil](https://sk1project.net/viewpage.php?page_id=21) [2](http://www.skencil.org/)
* [Slate](https://github.com/mitchcurtis/slate) OPEN-SOURCE (Windows, Linux, macOS) _(RECOMMENDED)_
* [SmoothDraw](http://www.smoothdraw.com/sd) FREEWARE - 3 versions (4.1.4 Beta-forWin7SP1-Win10, 4.0.5-for-WinXP-SP3-VistaSP1-Win7, and 3.2.11-for-2000/XP/Vista/Win7) _(RECOMMENDED)_
* [SpeedyPainter (OpenGLv3-based)](http://speedypainter.altervista.org/) [2](http://speedypainter.altervista.org/download/) FREEWARE _(HIGHLY RECOMMENDED)_
* [Spine](?), Creature, and Anima2D (all commercial)
* [Sprite Pad](?)
* [SpriteLand Spritemaker](http://www.spriteland.com/apps/spritemaker.html) FREEWARE
* [SpriteMate.com (online, duh)](spritemate.com)
* [Spriter Pro](https://steamcommunity.com/app/332360) (commercial)
* [Spriter2Unity](deadlink?)
* [Sprytile](https://github.com/Sprytile/Sprytile)
* [SwankyPaint](http://dime.lo4d.net/dl/swpaint) (Linux, Raspberry Pi, Android, Windows & Mac) [2](http://store.steampowered.com/app/432030/Swanky_Paint/) [3](http://paint.wetgenes.com/welcome) [4](https://github.com/xriss/swankypaint) [5](http://paint.wetgenes.com/dumid?continue=http://paint.wetgenes.com/paint/draw) [6](https://store.steampowered.com/app/432030/Swanky_Paint/) _(HIGHLY RECOMMENDED)_
* [Synfig Studio](https://www.synfig.org/) _(RECOMMENDED)_
* [Tablet Pressure Curve Tool (for Wacom tablets, 3rd-party app](https://blackink.cz/tablet-pressure-curve-tool/) _NOT AN IMAGE EDITOR, FREEWARE_ (useful for fine-tuning the pressure sensitivity of Wacom Tablets if the Wacom tablet-settings apps aren't enough for your needs)
* [Triangulart](https://maxwellito.github.io/triangulart/)
* [Tupi](https://www.linux-apps.com/p/1131159/)
* [TuxPaint (for kids/children)](http://www.tuxpaint.org/)
* [TwistedBrush](?) (commercial)
* [UberPaint 4P (4P), see GraFX2 for a more polished pixel-art editor) (alpha-version-quality pixel-art editor)](http://umlautllama.com/projects/4p/) [2](http://sourceforge.net/projects/uberpaint)
* [Unity UPA Toolkit (pixel-art editor)](https://assetstore.unity.com/packages/tools/painting/upa-toolkit-pixel-art-editor-27680)
* [Verve Painter (OpenGLv3.2+, by Taron (Timur Baysal), comparable to SpeedyPainter!)](https://www.taron.de/verve/verve_download.html) [2](http://www.taron.de/forum/viewtopic.php?f=4&t=4&sid=07a35f6881f428464a8cb44b300a8d81), look@taron.de _(HIGHLY RECOMMENDED)_
* [Wacintaki Poteto (oekaki)](http://www.ninechime.com/products/) [2](http://ninechime.com/deep/faq.php)
* [WnSoft PixBuilder Studio](https://www.wnsoft.com/en/pixbuilder/) FREEWARE
* [XPaint](http://sf-xpaint.sourceforge.net/) [2](https://sourceforge.net/projects/sf-xpaint/) OPEN-SOURCE
* [YellowBites WonderBrush (ONLY for BeOS and HaikuOS!)](http://yellowbites.com/wonderbrush.html)
* [](?) (?)

#### TO ADD TO THE ABOVE LIST & THEN SORT IT AGAIN ####
* see the invisible HTML-comment section hidden in the webpages' source-code below (e.g. just click the 'Raw' button on the GitHub page)...

<!--

Skencil, Pinta, TuxPaint, Karbon, VIPS, Blender3D, Digikam, KolourPaint, Inkscape, sK1, GPaint, CinePaint, LazPaint, RawTherapee, Darktable, Pinta, [Vectr](https://vectr.com/downloads/), [Fotoxx](https://kornelix.net/downloads/downloads.html), Pixelitor, 

* [Pixer (Qt-based pixel-art editor)](https://github.com/SilangQuan/Pixer) ([ipixer at SourceForge.net](https://sourceforge.net/projects/ipixer/))
* [Pixel](https://github.com/tjhancocks/Pixel) (a basic pixel-art editor written in Swift)
* [Pixel it](https://github.com/tavioalves/pixel-it) (a basic web-app to create, share or download pixel art around the web)
* [PixelArtShader](https://github.com/kushi34123616bd/PixelArtShader) (Render like * PixelArts from 3D model!!! It can be used to make 2D animations from pixel-art frames after exporting from a rotated/slightly-changed 3D-model!!!)
* [https://github.com/cwkx/godot-pixel-painter](https://github.com/cwkx/godot-pixel-painter)

EasyPaint (MS Paint-like, Qt/KDE-perfect)
MQ Sprite
mtPixy (by Mark Tyler, the author of mtPaint)
Pixelaria
lospec.com online pixel editor
PixelMatorPro (commercial digital-painting app)
PowerPaint
pyqx
qubicle (Voxel editor)
Blit (by 16bpp)
[PaintstormStudio](http://paintstormstudio.com) (commercial)

* [Pixavoxet](https://github.com/mishapsi/pixavoxet) (Voxel to Pixel Art Animator/Renderer inside Godot http://myskamyska.tumblr.com)

https://github.com/Hopson97/Pixelator
https://github.com/saint11/VectorChimera
https://github.com/Broxxar/PaletteSwapping
https://github.com/nathanharper/phixelgator

https://github.com/Convicted202/PixelShape
https://github.com/Soreine/pixel-art-maker (Generate palettes and dither any image with custom patterns)

* [Pixelm](https://github.com/shuhei/pixelm) ([Pixelm app](https://shuheikagawa.com/pixelm/))

https://github.com/sahwar/pixelRestorer
https://github.com/sahwar/blender-addon-import-pixelart
http://waifu2x.udp.jp/
https://github.com/sean-codes/pixeling

https://github.com/mananapr/pxlart
https://github.com/dylanaraps/pxltrm
https://github.com/alexpnt/pixel-art
https://github.com/SorenSaket/Pixel-Colors

[pyqx](https://github.com/MikiLoz92/pyqx) (a multiplatform graphics editor developed in Python and PyQt. It focuses in Pixel Art) OPEN-SOURCE _RECOMMENDED_

https://github.com/foldi/Big-Block-Framework
https://github.com/SorenSaket/Pixel-Colors (pixel-art color palettes)
https://github.com/mitaki28/pixcaler

https://github.com/59naga/jaggy
https://github.com/michaelrbk/pixelart-vectorizer
[Dithermark](https://github.com/allen-garvey/dithermark) (Transform your photos into pixel art https://app.dithermark.com)
[pixiEditor](https://github.com/flabbet/PixiEditor) (a lightweighted pixel art creator.)

https://github.com/g-whiz/pxSort
[Markov chains](https://en.wikipedia.org/wiki/Markov_chain), [fast Fourier transform](https://en.wikipedia.org/wiki/Fast_Fourier_transform), etc.
https://github.com/joshavanier/poxel
https://github.com/nikoferro/react-pixelify
https://github.com/BrandonHilde/PixelArt
[Pixelaria (written in .NET C# 4.5)](https://github.com/LuizZak/Pixelaria) (C# program for creating pixel art game sprites) OPEN-SOURCE _RECOMMENDED_
https://github.com/guastallaigor/vue-pixel-art
https://github.com/qbart/isometric-pixel-art
MORE (research...): https://github.com/search?p=17&q=pixel+art&type=Repositories

https://www.techlinu.com/3-must-linux-apps/
https://conceptartempire.com/photoshop-linux-alternatives/
https://conceptartempire.com/pixel-art-software/
https://conceptartempire.com/digital-painting-software/
https://conceptartempire.com/storyboard-software/
http://forum.tabletpcreview.com/threads/list-of-programs-for-the-artist.36406/
https://forum.mxlinux.org/viewtopic.php?t=42013
http://archive.is/WLFtm
https://ja.osdn.net/projects/azpainter/#gallery

------

https://github.com/david95thinkcode/SimplePaintApp
https://github.com/smay1613/Qt-Paint
https://github.com/Abdelrhman-Yasser/Paint
https://github.com/isac322/paint
https://github.com/plumlike/Simple-Paint-Program
https://github.com/DaniloNovakovic/WinFormsFun
https://github.com/shad0w-13/MiniPaint
https://github.com/TimmyRB/Paint
https://github.com/Ankitzero/rabbitpaint
https://github.com/shawsourav/Paint-JS
https://github.com/nhnent/tui.image-editor
https://github.com/dnchia/Ion3Paint
https://github.com/zakee94/paintJOGL
https://github.com/saifkhichi96/paint
https://github.com/haxxorsid/swing-paint-application
https://github.com/cojapacze/sketchpad
https://github.com/shawsourav/Paint-JS

------

* [Skencil]()
* [Pinta]()
* [TuxPaint]()
* [Karbon]()
* [VIPS]()
* [Blender3D]()
* [Digikam]()
* [KolourPaint]()
* [Inkscape]()
* [sK1]() (vector graphics editor) OPEN-SOURCE _RECOMMENDED_
* [GPaint]()
* [CinePaint]()
* [LazPaint]()
* [RawTherapee]()
* [Darktable]()
* [Pinta]()
* [Vectr](https://vectr.com/downloads/)
* [Fotoxx](https://kornelix.net/downloads/downloads.html)
* Pixelitor
* [Pixelator](https://github.com/Hopson97/Pixelator)
* [Pixer (Qt-based pixel-art editor)](https://github.com/SilangQuan/Pixer) ([ipixer at SourceForge.net](https://sourceforge.net/projects/ipixer/))
* [Pixel](https://github.com/tjhancocks/Pixel) (a basic pixel-art editor written in Swift)
* [Pixel it](https://github.com/tavioalves/pixel-it) (a basic web-app to create, share or download pixel art around the web)
* [PixelArtShader](https://github.com/kushi34123616bd/PixelArtShader) (Render like * PixelArts from 3D model!!! It can be used to make 2D animations from pixel-art frames after exporting from a rotated/slightly-changed 3D-model!!!)
* [godot-pixel-painter](https://github.com/cwkx/godot-pixel-painter)
* [Pixelaria (written in .NET C# 4.5)](https://github.com/LuizZak/Pixelaria) (C# program for creating pixel art game sprites) OPEN-SOURCE _RECOMMENDED_
* [EasyPaint]() (MS Paint-like, Qt/KDE-perfect)
* [MQ Sprite]()
* [mtPixy]() (by Mark Tyler, the original author of mtPaint)
* [Pixelaria]()
* [lospec.com online pixel editor]()
* [PixelMatorPro]() (commercial digital-painting app)
* [PowerPaint]()
* [pyqx](https://github.com/MikiLoz92/pyqx) (a multiplatform graphics editor developed in Python and PyQt. It focuses in Pixel Art) OPEN-SOURCE _RECOMMENDED_
* [qubicle]() (Voxel editor)
* [Blit (by 16bpp)]()
* [PaintstormStudio](http://paintstormstudio.com) (commercial digital-painting app)
* [Pixavoxet](https://github.com/mishapsi/pixavoxet) (Voxel to Pixel Art Animator/Renderer inside Godot),http://myskamyska.tumblr.com)
* [pixiEditor](https://github.com/flabbet/PixiEditor) (a lightweighted pixel art creator) OPEN-SOURCE
* [pxlart](https://github.com/mananapr/pxlart)
* [pxltrm](https://github.com/dylanaraps/pxltrm)
* [pixel-art](https://github.com/alexpnt/pixel-art)
* [Pixel-Colors](https://github.com/SorenSaket/Pixel-Colors)


* https://github.com/sean-codes/pixeling





* [VectorChimera](https://github.com/saint11/VectorChimera) (a palette swapper software for pixel art)
* [PaletteSwapping](https://github.com/Broxxar/PaletteSwapping)
* [phixelgator](https://github.com/nathanharper/phixelgator) (convert photos to pixel art at the command line.)

* [PixelShape](https://github.com/Convicted202/PixelShape) (a web-based pixel editor that comes in handy when creating pixel art images and animations)
* [pixel-art-maker](https://github.com/Soreine/pixel-art-maker) (Generate palettes and dither any image with custom patterns)
* [Pixelm](https://github.com/shuhei/pixelm) ([Pixelm app](https://shuheikagawa.com/pixelm/))

* [pixelRestorer](https://github.com/sahwar/pixelRestorer) (Using statistics to restore pixel art images)
* [Blender-addon-import-pixelart](https://github.com/sahwar/blender-addon-import-pixelart)



* [Big-Block-Framework](https://github.com/foldi/Big-Block-Framework) (A JavaScript framework for rendering pixel art, pixel fonts and particles in a web browser - http://www.bigblockframework.com)
* [Pixel-Colors](https://github.com/SorenSaket/Pixel-Colors) (pixel-art color palettes)
* [pixcaler](https://github.com/mitaki28/pixcaler) (a pixel-art upscaler based on pix2pix network - demo: http://mitaki28.info/pixcaler/)
* [jaggy](https://github.com/59naga/jaggy) (Keep the sharpness for Pixel-Art)
* [pixelart-vectorizer](https://github.com/michaelrbk/pixelart-vectorizer) (Convert pixel art to SVG vector image)
* [Dithermark](https://github.com/allen-garvey/dithermark) (Transform your photos into pixel art - https://app.dithermark.com)
* [pxSort](https://github.com/g-whiz/pxSort) (A Google Android application for creating "pixel sorting" glitch art.)



https://github.com/david95thinkcode/SimplePaintApp
https://github.com/smay1613/Qt-Paint
https://github.com/Abdelrhman-Yasser/Paint
https://github.com/isac322/paint
https://github.com/plumlike/Simple-Paint-Program
https://github.com/DaniloNovakovic/WinFormsFun
https://github.com/shad0w-13/MiniPaint
https://github.com/TimmyRB/Paint
https://github.com/Ankitzero/rabbitpaint
https://github.com/shawsourav/Paint-JS
https://github.com/nhnent/tui.image-editor
https://github.com/dnchia/Ion3Paint
https://github.com/zakee94/paintJOGL
https://github.com/saifkhichi96/paint
https://github.com/haxxorsid/swing-paint-application
https://github.com/cojapacze/sketchpad
https://github.com/shawsourav/Paint-JS



https://github.com/joshavanier/poxel
https://github.com/nikoferro/react-pixelify
https://github.com/BrandonHilde/PixelArt

https://github.com/guastallaigor/vue-pixel-art
https://github.com/qbart/isometric-pixel-art
MORE (research...): https://github.com/search?p=17&q=pixel+art&type=Repositories

https://www.techlinu.com/3-must-linux-apps/
https://conceptartempire.com/photoshop-linux-alternatives/
https://conceptartempire.com/pixel-art-software/
https://conceptartempire.com/digital-painting-software/
https://conceptartempire.com/storyboard-software/
http://forum.tabletpcreview.com/threads/list-of-programs-for-the-artist.36406/
https://forum.mxlinux.org/viewtopic.php?t=42013
http://waifu2x.udp.jp/
http://archive.is/WLFtm
https://ja.osdn.net/projects/azpainter/#gallery

------

* [Skencil]()
* [Pinta]()
* [TuxPaint]()
* [Karbon]()
* [VIPS]()
* [Blender3D]()
* [Digikam]()
* [KolourPaint]()
* [Inkscape]()
* [sK1]() (vector graphics editor) OPEN-SOURCE _RECOMMENDED_
* [GPaint]()
* [CinePaint]()
* [LazPaint]()
* [RawTherapee]()
* [Darktable]()
* [Pinta]()
* [Vectr](https://vectr.com/downloads/)
* [Fotoxx](https://kornelix.net/downloads/downloads.html)
* Pixelitor
* [Pixelator](https://github.com/Hopson97/Pixelator)
* [Pixer (Qt-based pixel-art editor)](https://github.com/SilangQuan/Pixer) ([ipixer at SourceForge.net](https://sourceforge.net/projects/ipixer/))
* [Pixel](https://github.com/tjhancocks/Pixel) (a basic pixel-art editor written in Swift)
* [Pixel it](https://github.com/tavioalves/pixel-it) (a basic web-app to create, share or download pixel art around the web)
* [PixelArtShader](https://github.com/kushi34123616bd/PixelArtShader) (Render like * PixelArts from 3D model!!! It can be used to make 2D animations from pixel-art frames after exporting from a rotated/slightly-changed 3D-model!!!)
* [godot-pixel-painter](https://github.com/cwkx/godot-pixel-painter)
* [Pixelaria (written in .NET C# 4.5)](https://github.com/LuizZak/Pixelaria) (C# program for creating pixel art game sprites) OPEN-SOURCE _RECOMMENDED_
* [EasyPaint]() (MS Paint-like, Qt/KDE-perfect)
* [MQ Sprite]()
* [mtPixy]() (by Mark Tyler, the original author of mtPaint)
* [Pixelaria]()
* [lospec.com online pixel editor]()
* [PixelMatorPro]() (commercial digital-painting app)
* [PowerPaint]()
* [pyqx](https://github.com/MikiLoz92/pyqx) (a multiplatform graphics editor developed in Python and PyQt. It focuses in Pixel Art) OPEN-SOURCE _RECOMMENDED_
* [qubicle]() (Voxel editor)
* [Blit (by 16bpp)]()
* [PaintstormStudio](http://paintstormstudio.com) (commercial digital-painting app)
* [Pixavoxet](https://github.com/mishapsi/pixavoxet) (Voxel to Pixel Art Animator/Renderer inside Godot),http://myskamyska.tumblr.com)
* [pixiEditor](https://github.com/flabbet/PixiEditor) (a lightweighted pixel art creator) OPEN-SOURCE
* [pxlart](https://github.com/mananapr/pxlart)
* [pxltrm](https://github.com/dylanaraps/pxltrm)
* [pixel-art](https://github.com/alexpnt/pixel-art)
* [Pixel-Colors](https://github.com/SorenSaket/Pixel-Colors)


* https://github.com/sean-codes/pixeling





* [VectorChimera](https://github.com/saint11/VectorChimera) (a palette swapper software for pixel art)
* [PaletteSwapping](https://github.com/Broxxar/PaletteSwapping)
* [phixelgator](https://github.com/nathanharper/phixelgator) (convert photos to pixel art at the command line.)

* [PixelShape](https://github.com/Convicted202/PixelShape) (a web-based pixel editor that comes in handy when creating pixel art images and animations)
* [pixel-art-maker](https://github.com/Soreine/pixel-art-maker) (Generate palettes and dither any image with custom patterns)
* [Pixelm](https://github.com/shuhei/pixelm) ([Pixelm app](https://shuheikagawa.com/pixelm/))

* [pixelRestorer](https://github.com/sahwar/pixelRestorer) (Using statistics to restore pixel art images)
* [Blender-addon-import-pixelart](https://github.com/sahwar/blender-addon-import-pixelart)



* [Big-Block-Framework](https://github.com/foldi/Big-Block-Framework) (A JavaScript framework for rendering pixel art, pixel fonts and particles in a web browser - http://www.bigblockframework.com)
* [Pixel-Colors](https://github.com/SorenSaket/Pixel-Colors) (pixel-art color palettes)
* [pixcaler](https://github.com/mitaki28/pixcaler) (a pixel-art upscaler based on pix2pix network - demo: http://mitaki28.info/pixcaler/)
* [jaggy](https://github.com/59naga/jaggy) (Keep the sharpness for Pixel-Art)
* [pixelart-vectorizer](https://github.com/michaelrbk/pixelart-vectorizer) (Convert pixel art to SVG vector image)
* [Dithermark](https://github.com/allen-garvey/dithermark) (Transform your photos into pixel art - https://app.dithermark.com)
* [pxSort](https://github.com/g-whiz/pxSort) (A Google Android application for creating "pixel sorting" glitch art.)



https://github.com/david95thinkcode/SimplePaintApp
https://github.com/smay1613/Qt-Paint
https://github.com/Abdelrhman-Yasser/Paint
https://github.com/isac322/paint
https://github.com/plumlike/Simple-Paint-Program
https://github.com/DaniloNovakovic/WinFormsFun
https://github.com/shad0w-13/MiniPaint
https://github.com/TimmyRB/Paint
https://github.com/Ankitzero/rabbitpaint
https://github.com/shawsourav/Paint-JS
https://github.com/nhnent/tui.image-editor
https://github.com/dnchia/Ion3Paint
https://github.com/zakee94/paintJOGL
https://github.com/saifkhichi96/paint
https://github.com/haxxorsid/swing-paint-application
https://github.com/cojapacze/sketchpad
https://github.com/shawsourav/Paint-JS



https://github.com/joshavanier/poxel
https://github.com/nikoferro/react-pixelify
https://github.com/BrandonHilde/PixelArt

https://github.com/guastallaigor/vue-pixel-art
https://github.com/qbart/isometric-pixel-art
MORE (research...): https://github.com/search?p=17&q=pixel+art&type=Repositories

https://www.techlinu.com/3-must-linux-apps/
https://conceptartempire.com/photoshop-linux-alternatives/
https://conceptartempire.com/pixel-art-software/
https://conceptartempire.com/digital-painting-software/
https://conceptartempire.com/storyboard-software/
http://forum.tabletpcreview.com/threads/list-of-programs-for-the-artist.36406/
https://forum.mxlinux.org/viewtopic.php?t=42013
http://waifu2x.udp.jp/
http://archive.is/WLFtm
https://ja.osdn.net/projects/azpainter/#gallery

https://osdn.net/projects/gimp-painter/releases/p6799
https://github.com/seagetch/gimp-painter
http://detstwo.com/sai/

------

---

Solarized dark             |  Solarized Ocean
:-------------------------:|:-------------------------:
![alt text](http://url/to/img.png)  |  ![](https://...Dark.png)
---
<p float="left">
  <img src="/img1.png" width="100" />
  <img src="/img2.png" width="100" /> 
  <img src="/img3.png" width="100" />
</p>
---
<img src="https://openclipart.org/image/2400px/svg_to_png/28580/kablam-Number-Animals-1.png" width="200"/> <img src="https://openclipart.org/download/71101/two.svg" width="300"/>
---
# Title

<img align="left" src="./documentation/images/A.jpg" alt="Made with Angular" title="Angular" hspace="20"/>
<img align="left" src="./documentation/images/B.png" alt="Made with Bootstrap" title="Bootstrap" hspace="20"/>
<img align="left" src="./documentation/images/C.png" alt="Developed using Browsersync" title="Browsersync" hspace="20"/>
<br/><br/><br/><br/><br/>

## Table of Contents... ##

-->

###  TO DO ###
* Sort the list = DONE?
* Add more entries to the list, including those suggested by others (e.g.
https://en.wikipedia.org/wiki/Comparison_of_raster_graphics_editors#List
https://en.wikipedia.org/wiki/List_of_vector_graphics_editors
https://amp.reddit.com/r/photography/comments/a6alpu/the_ultimate_list_of_photo_editing_tools_in_2018
https://alternativeto.net/software/gimp/
)
* Cleanup, remove/Merge duplicate entries, fix typos&formatting, and add missing hyperlinks
* Add a new category for Google Android & Apple iOS apps
* Maybe add a category for apps for exotic and niche OSs
* Separate commericial apps into a separate category
* Maybe make the list into a Markdown table
* Add screenshots for each app = TEDIOUS, IT WILL TAKE A LOT OF TIME. For now just use the RECOMMENDED & HIGHLY RECOMMENDED apps in the list...
* Maybe add FOSS 3D software as a separate category, e.g. [Blender 3D](https://www.blender.org/), and also FOSS pixel-art-like voxel editors (e.g. [MagicaVoxel](https://ephtracy.github.io/) [2](http://www.voxelmade.com/magicavoxel/) [3](https://www.rollapp.com/app/magicavoxel) [4](https://sketchfab.com/tags/magicavoxel) (+[IsoVoxel](https://github.com/tommyettinger/IsoVoxel) 3D-voxel TO isometric pixel-art plug-in))?
