# OSs (operating systems) worth using in 2019 and beyond #

Списък с операционни системи, които си заслужава да се използват през 2019 г. и занапред...

## The Big Holy Trinity of Desktop OSs ##
_**Microsoft (MS) Windows**_ (XP SP3 (obsolete except for some compatibility with older apps&older video-games); **7 SP1 (Pro/Ultimate x64)**, (8.1), **10**) (important related FOSS projects: see below), _**(GNU/)Linux**_ (Linux distros recommended by @sahwar for use in 2019: see below), and _**macOS**_ (X+; [Homebrew](https://brew.sh/), [MacPorts](http://macports.org/), Wine for macOS, [KDE apps for macOS X](https://community.kde.org/Mac)).

### Important MS Windows-related Microsoft & FOSS projects ###
**[ReactOS](http://reactos.org/)**, **[Wine](https://www.winehq.org/)/PlayOnLinux**, [Cygwin](http://www.cygwin.com/), [MinGW](http://www.mingw.org/), [MinGW-w64](https://mingw-w64.org/doku.php/start), MSVC, Linux Subsystem for Windows10, [Chocolatey](http://chocolatey.org/), [NuGet](http://nuget.org/), OneGet (Windows10), `cmd.exe`&`PowerShell`, [Win-Builds](http://win-builds.org/doku.php), [KDE apps for MS Windows](https://community.kde.org/Windows) [2](https://techbase.kde.org/Getting_Started/Build/Historic/KDE4_Windows), [BusyBox](https://busybox.net/), etc.

#### Полезен софтуер за `cmd.exe`/PowerShell & Windows Explorer ####
`cmd.exe`, PowerShell / PowerShell Core (open-source), [ConEmu](https://conemu.github.io/), [Process Explorer](https://docs.microsoft.com/en-us/sysinternals/downloads/process-explorer), [Clink](http://mridgers.github.io/clink), [PSReadLine](https://github.com/lzybkr/PSReadLine), [PSGet](https://github.com/psget/psget), Chocolatey / NuGet / MS [OneGet](https://github.com/OneGet/oneget), [Process Hacker](http://processhacker.sourceforge.net/), [cmder](https://cmder.net/), [GitForWindows](https://gitforwindows.org/), [mintty](https://mintty.github.io/), [wsltty](https://github.com/mintty/wsltty), [NirSoft apps](http://www.nirsoft.net/) (especially NirCmd), [Windows utilities by Code.Kliu.org](http://code.kliu.org/) (like [elevate](http://code.kliu.org/misc/elevate/)), [Babun](https://github.com/babun/babun), plus Windows (x64) builds of `curl`, `wget`, `ffmpeg`/`libav`, etc.

### Viable Linux distros 2019 ###
* [MX Linux](http://mxlinux.org/) 10/10
* [AntiX](https://antixlinux.com/) 8/10
* [Debian](http://debian.org/) (newest; +squeeze-backports and later) 8/10
* [Devuan](https://devuan.org/) (Debian minus `systemd`) 9/10
* [Linux Mint](http://linuxmint.com/) 10/10 (XFCE DE, MATE DE, Cinnamon DE, LMDE (Debian-based rolling Linux Mint)) 9/10
* [Ubuntu](http://ubuntu.com/) 9/10 ([Xubuntu](http://xubuntu.org/), [Lubuntu](https://lubuntu.net/), [Ubuntu MATE](http://ubuntu-mate.org/), [Kubuntu](http://kubuntu.org/), Ubunty Unity (~dead)) 9/10
* [USU](http://learnfree.eu/?lang=bg) (**Ubuntu-based Linux distro oriented towards Bulgarian users!!!**) 8/10
* [FerenOS](https://ferenos.weebly.com/get-it.html) [(download)](https://ferenos.weebly.com/get-it.html), [FerenOS at SourceForge](https://sourceforge.net/projects/ferenoslinux/), [FerenOS Next at SourceForge.net](https://sourceforge.net/projects/feren-os-development-builds/), [FerenOS Transfer Tool](https://ferenos.weebly.com/feren-os-transfer-tool.html) 9/10
* [Arch Linux](http://archlinux.org/) + [AUR](https://aur.archlinux.org/) 8/10
* [Fedora](https://getfedora.org/) 7.5/10
* [Gentoo](https://www.gentoo.org/)
* [OpenSUSE](https://www.opensuse.org/) (12.3 or later; OpenSUSE Tumbleweed)
* [RHEL (RedHat Enterprise Linux)](https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux) & [CentOS](https://www.centos.org/) (suitable for servers, Debian, Ubuntu, Arch Linux, and Gentoo - are the other Linux distros popular for servers); Oracle Linux... 10/10
* [Void Linux](https://voidlinux.org/) 9/10 (with the XBPS package-system) 7.5/10
* [Alpine Linux](https://www.alpinelinux.org/)
* [GuixSD](https://www.gnu.org/software/guix/)
* [NixOS](https://nixos.org/)
* [Puppy Linux](http://puppylinux.org/main/Overview%20and%20Getting%20Started.htm) (especially the Ubuntu/Debian-based LTS builds!) 9/10
* [ElementaryOS](https://elementary.io/bg/) 8/10
* [SliTaz GNU/Linux](http://www.slitaz.org/en/) 7.5/10
* [CLIP OS](https://clip-os.org/en/) (by the ANSSI (National Cybersecurity Agency of France))
* [Slax](https://www.slax.org/)
* [Solus Linux](https://getsol.us/home/)
* [Bodhi Linux](https://www.bodhilinux.com/)
* [Trisquel Linux](https://trisquel.info/en)
* [Manjaro Linux](https://manjaro.org/)
* [Sabayon Linux](https://www.sabayon.org/)
* [Mageia Linux](http://www.mageia.org/bg/)
* [Kali Linux](https://www.kali.org/)
* [CoreOS](https://coreos.com/)
* [Slackware Linux](http://www.slackware.com/)
* [Minimal Linux Live](http://minimal.linux-bg.org/#home)
* [YoctoProject](https://www.yoctoproject.org/)
* [LXLE](http://www.lxle.net/)
* [PureOS](https://www.pureos.net/) (from https://puri.sm/, powerful libre laptops!)
* [ZorinOS](https://zorinos.com/)

...

* [Linux Lite](https://www.linuxliteos.com/download.html)
* [Lubuntu](http://lubuntu.net/) 9/10
* [Trisquel Mini](https://trisquel.info/en/wiki/trisquel-mini)
* [BodhiLinux](http://www.bodhilinux.com/)
* [ChromiumOS/ChromeOS](https://www.chromium.org/chromium-os)
* [SolusOS](https://solus-project.com/)
* [DamnSmallLinux](https://distrowatch.com/table.php?distribution=damnsmall)
* [Porteus Linux](http://www.porteus.org/)
* [wattOS Linux](http://planetwatt.com/new/)
* [4MLinux](http://4mlinux.com/) 8/10
* [EasyOS](https://easyos.org/) (based on Puppy Linux)
* [ToriOS](http://torios.top/)
* [Linux From Scratch (LFS)](http://www.linuxfromscratch.org/)
* [TinyCoreLinux](http://www.tinycorelinux.net/)
* [deepin Linux](https://www.deepin.org/en/) 8/10
* [Slackware Linux](http://www.slackware.com/)
* [LXLE](http://lxle.net/) 8/10
* [SteamOS](http://store.steampowered.com/steamos/)
* [Trisquel Linux](https://trisquel.info)
* [Sabayon](https://www.sabayon.org/)
* [PeppermintOS](https://peppermintos.com/)
* PCLinuxOS
* Raspbian
* Scientific Linux
* Turbolinux
* Mandriva
* Asianux
* CloudLinux
* PLD Linux
...
* [Parabola GNU/Linux](https://www.parabola.nu/)
* [gNewSense GNU/Linux](http://www.gnewsense.org/)
* [Dynebolic GNU/Linux](https://www.dyne.org/software/dynebolic/)
* [Dragora GNU/Linux-libre](http://www.dragora.org/repo.fsl/doc/trunk/www/index.md)
* [Blag GNU/Linux](http://www.blagblagblag.org/) (based on Fedora)
* [Freed-ora GNU/Linux](http://www.fsfla.org/ikiwiki/selibre/linux-libre/freed-ora.en.html) (based on Fedora)
* [GNU Linux-libre](http://linux-libre.fsfla.org/pub/linux-libre/) ([info](http://www.fsfla.org/ikiwiki/selibre/linux-libre/index.en.html))
* [Ututo GNU/Linux](http://ututo.org/)
* [libeRTy for Linux-libre](http://www.fsfla.org/ikiwiki/selibre/linux-libre/liberty.en.html)
* [Ezix Linux](https://ezix.org/project/)
* For more Linux distros, check [DistroWatch.com (Linux-distro popularity 'rankings')](https://distrowatch.com/), ([ibiblio Linux-distro mirrors](http://distro.ibiblio.org/), or just do a web-search with your favorite web-search engine (e.g. google.com, [ixquick.com / startpage.com](https://www.startpage.com/), duckduckgo.com, yahoo.com, bing.com, etc.).

Searching for **packages for (GNU/)Linux distros**:
https://pkgs.org (!!!)
https://packages.ubuntu.com/
https://www.debian.org/distrib/packages
https://www.archlinux.org/packages/
https://aur.archlinux.org/
http://packages.linuxmint.com/
https://launchpad.net (Ubuntu-specific, Linux Mint-specific, and Debian-specific .deb/source PPAs, e.g. https://launchpad.net/lazpaint, https://launchpad.net/openshot, https://launchpad.net/gnome-paint)
https://apps.fedoraproject.org/packages/
https://copr.fedorainfracloud.org/ (!!! Includes distro-agnostic AppImage's for apps!)
https://rpmfusion.org/
https://src.fedoraproject.org/
https://packages.gentoo.org/
https://rpms.remirepo.net/rpmphp/
https://getfedora.org/keys/
...
https://distrowatch.com/packages.php
https://freshfoss.com/
https://www.icewalkers.com/


### Other notable UNIX-like OSs apart from (GNU/)Linux (primarily used for servers) ###
**FreeBSD**, **OpenBSD**, NetBSD, DragonflyBSD, etc.

## The mainstream OSs for mobile devices (smartphones, phablets, tablets) ##
_**Google Android**_, _**Apple iOS**_, _**Windows10 mobile**_, _**Samsung Tizen**_, _**[SailfishOS](https://sailfishos.org/)**_ (by Jolla, with a heritage from Nokia and MeeGo), maruOS, BlissOS, Mer Project, Ubuntu Touch, KaiOS, KDE Plasma Mobile (Qt5-based?), postmarketOS, PureOS Librem, LineageOS, eelo, etc.

## Older OSs for older mobile devices ##
SymbianOS 10, WinCEv5&v6 (Windows Embedded Compact) (all are obsolete except for older mobile devices or for hobbyist projects involving them), JavaME-based old cell-phones' custom OSs, etc.

## Some other notable OSs ##
**[Google ChromeOS](https://www.google.com/chromebook/)** / **[Chromium OS](https://en.wikipedia.org/wiki/Chromium_OS) [2](https://www.chromium.org/chromium-os)**, **[HaikuOS](https://www.haiku-os.org/) (inspired by BeOS)**, [IllumosOS](https://www.illumos.org/projects) (OpenIndiana=OpenSolaris continuation), OpenCSW (Oracle Solaris 10 Update 8 or later), Oracle Solaris (previously: Sun Solaris), [OpenIndiana](https://www.openindiana.org/download/), [MINIX3](http://www.minix3.org/), [Plan9](https://9p.io/plan9/index.html) (from Bell Labs), [Inferno OS](https://en.wikipedia.org/wiki/Inferno_(operating_system)), [HelenOS](http://www.helenos.org/), [OSv](http://osv.io/) [2](https://github.com/cloudius-systems/osv), BeOS, AmigaOS, OS/2, RISC OS, QNX, etc., {some micro FOSS-or-freeware small independent OSs to add}, etc.

## EXTRA - Emulators for old and legacy video-game consoles & ancient PC hardware ##
* There are too many (hundreds!) FOSS video-game-console emulators to list here, but [MAME](https://www.mamedev.org/) (especially MAMEUI64 and the older mame32.exe GUI: [1](https://github.com/mamedev/mame) [2](https://github.com/Robbbert/mameui) [3](https://www.afterdawn.com/software/system_tools/emulation/mame-for-windows.cfm) [4](http://www.emutopia.com/index.php/emulators/item/257-mame/157-mameui) [5](https://www.afterdawn.com/software/system_tools/emulation/mameui-64-bit.cfm) [6]() [MESSUI](https://github.com/Robbbert/messui) [2](http://mess.redump.net/start) [3](http://messui.1emulation.com/) [4](http://www.progettosnaps.net/mameui/) [5](http://www.progettoemma.net/mess/extra.html) [6](http://mrdo.mameworld.info/index.php), [IV/Play&MAMEUI64](http://www.mameui.info/)), [FreeDOS](https://www.freedos.org/), [DOXBox](https://www.dosbox.com/), [ScummVM](https://www.scummvm.org/) are among the most notable.

## EXTRA2 - Good computers for FLOSS/FOSS users ##
There appeared a number of new manufacturers&vendors of computers suitable for FLOSS users.
Apart from buying&using older second-hand laptops (IBM/Lenovo ThinkPad's, HP EliteBook, Fujitsu LifeBook, etc. brands), I find these 2 companies to be selling great laptops: https://puri.sm/ and https://system76.com/ (information accurate as of February 2019).
