
_THE LIST IS IN DEVELOPMENT, PLEASE CHECK BACK LATER FOR A MORE COMPLETE VERSION. THANK YOU._

### Linux (GNU/Linux) distros and other UNIX-like OSs ###
**App packaging/distribution/installation file-formats & app distribution-channels for (GNU/)Linux distros:**
* .deb, .rpm, etc. binary formats & related CLI&GUI app package-managers (`apt`/`apt-get`, `Synaptic`, `emerge (portage)`, `pacman` (Arch Linux), `zypper` (OpenSUSE), `nix-env` (NixOS Linux), etc.), downloaded from locally-installed package managers of your Linux distro OR downloaded from some obscure web-address...
* **AppImage**'s ([AppImageKit](https://github.com/AppImage/AppImageKit), https://appimage.org, https://appimage.github.io/apps/, [AppImageHub](https://appimage.github.io/), [AppImageLauncher](https://github.com/TheAssassin/AppImageLauncher) (https://www.opendesktop.org/p/1228228))
* **flatpak**'s (https://flatpak.org/, https://flathub.org/home)
* Ubuntu **snap**'s (https://snapcraft.io/), [snapd](https://github.com/snapcore/snapd)
* **source** .tar.gz, .tar.xz, .tar.7z files for compiling
* searching through the Linux-distro repositories via locally-installed package managers or searching through online package-system search-engine/databases frontends or searching through the Linux-distro's online directory of repositories of available distro packages: **https://pkgs.org/** (& related: http://www.getdeb.net/, http://www.rpmseek.com/index.html, https://build.opensuse.org/, etc.)
* [Linuxbrew](http://linuxbrew.sh) (Info: https://www.ostechnix.com/linuxbrew-common-package-manager-linux-mac-os-x/)
* Braumeister (defunct?): http://braumeister.org/formulae/
* Ubuntu LaunchPad.net **PPAs** (https://launchpad.net/)
* **BitBucket** (https://bitbucket.org/)
* **GitLab** (https://about.gitlab.com/), or your own (self-)hosted version of GitLab CE (Community Edition)...
* **BinTray** (https://bintray.com)
* JFrog BinTray (https://jfrog.com/bintray/)
* openSUSE Build Service (https://build.opensuse.org/)
* Fedora COPR AppImage's repos: https://copr.fedorainfracloud.org/coprs/
* [XBPS](https://voidlinux.org/usage/xbps/) & etc. package systems (?)...
* Python PIP (https://packaging.python.org/tutorials/installing-packages/, https://pypi.org/project/pip/), [https://packagist.org](Packagist) (the PHP programming-language's package-repository of PHP packages installable via [Composer](https://getcomposer.org/)), [crates.io (packages for the Rust programming language](https://crates.io/crates/smithay-clipboard), [YarnPKG](https://yarnpkg.com/lang/en/), TeX packages, etc.
* installing by getting the package via a version-control-system's graphical-app (i.e. Git, GitLab, GitHub, CVS, etc.)...
* [npm](https://www.npmjs.com/) (JavaScript packages)
* https://mesonbuild.com/
* https://ninja-build.org/
* running Windows `.exe`/`.msi` files via [Wine](https://www.winehq.org/)/PlayOnLinux (useful: https://www.dedoimedo.com/computers/wine-apps-hd-display.html, [DxWnd](https://sourceforge.net/p/dxwnd/home/Home/), etc.)
* etc. ...

### Windows (Microsoft Windows, XP & 7+) ###
* installing from physical-media `.exe`/`.msi` installers
* directly executing the app's `.exe` binary from its physical-media-copied OR Internet-downloaded archive (.zip, .7z, .tar.xz, .iso, etc.) unpacked into a new folder.
* Downloading `.exe`/`.msi` installers (or source-code for local compiling) from https://sourceforge.net/ (BEWARE if adware is bundled!), https://www.fosshub.com/, https://osdn.net, https://github.com, GitLab (e.g. https://gitlab.com/florian0)...
_Software download-portals to get ONLY FREEWARE and open-source apps from, NOT other apps:_
http://brothersoft.com/, https://www.kaldata.com/ (Bulgarian IT website), https://www.download.bg/ (Bulgarian IT website), https://www.softpedia.com/, https://www.techspot.com/downloads/, https://en.softonic.com/windows, https://filehippo.com/, http://www.oldapps.com/, https://www.old-games.com/, https://www.makeuseof.com/, https://www.howtogeek.com/, https://www.techspot.com/downloads/, http://www.tucows.com/downloads, https://download.cnet.com/windows/, https://www.techradar.com/, https://www.tomsguide.com/t/windows/... http://caiman.us/, http://homeoftheunderdogs.net/ (https://twitter.com/HotU_net), http://www.homeoftheunderdogs.net/company.php?name=Freeware, http://www.abandonwarering.com/, http://www.aplaces.net/, etc.
* **Package managers for Windows:**
  * * [Chocolatey](https://chocolatey.org)
  * * [NuGet](https://www.nuget.org) (.NET apps)
  * * [PackageManagement/OneGet](https://github.com/OneGet/oneget) (Windows 10/Windows Server 2016's built-in package manager manager)
  * * Python PIP (https://packaging.python.org/tutorials/installing-packages/, https://pypi.org/project/pip/)
  * * other external/3rd-party Windows GUI apps/package-managers for installing 3rd-party apps (application software)
* etc.

### macOS (Apple macOS) ###
* installing the macOS app from its macOS-centric .dmg binary/installer (from local physical-media or downloaded from Internet-sources)
* [homebrew](https://brew.sh/) (package manager for macOS), [Homebrew Formulae](https://formulae.brew.sh/)
* [macports](https://www.macports.org/)
* running Windows .exe/.msi files via [Wine](https://www.winehq.org/)/PlayOnLinux for macOS
