# Bulogos
([sync this GitHub repo with the GitLab mirror repo](https://moox.io/blog/keep-in-sync-git-repos-on-github-gitlab-bitbucket/))
### Bulogos /bɤlɔgɔs/, /bɤləgɔs/ (Бълогос / БълЪгос); The Bulogos Project; Project Bulogos
:us: :gb: Useful information for Bulgarian-language users of Linux and FOSS/FLOSS (Free (, libre) and open-source software), among other things.

### Bulogos /bɤlɔgɔs/, /bɤləgɔs/ (Бълогос / БълЪгос); Проект „Бълогос“ (Проект „БълЪгос“, Проект Bulogos)
🇧🇬 &#x1F1E7;&#x1F1EC; Полезна информация за българоезични ползватели на Линукс и ССиСсОК (свободен софтуер и софтуер с отворен код), измежду други неща.
