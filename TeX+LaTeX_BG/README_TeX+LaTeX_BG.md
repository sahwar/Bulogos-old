Тук ще сложа на едно място полезна информация за писане на TeX/LaTeX файлове, включително помощни указания, написани от други (във формат на .PDF документи).

TeX е система за произвеждане на красиви документи, разработена от Доналд Кнут (Donald Knuth). Представлява маркиращ език (markup language) за форматиране на обикновен текст, плюс програми, които конвертират (преобразуват) изходен код, написан на този маркиращ език... в красив .PDF документ, с красиви шрифтове, специално изработени за употреба от TeX/LaTeX.

Системата е изключително подходяща за писане и публикуване на научни статии, поради добрата си поддръжка на знаци за математическа нотация, както и това, че крайният документ е красив PDF готов за печатане (или добавяне към по-голяма компилация от документи, слята в голям PDF файл).

С подходящи допълнителни пакети, описани в изходния .tex файл, можете да добавите и поддръжка на кирилица, на български език, както и други допълнителни екстри.

**Вижте също:**
* _TUG-BG (TeX User Group for Bulgarians)_ в Google Groups.
* https://www.ctan.org/tex-archive/language/hyph-utf8
* https://tug.org/applications/pdftex/
* https://ctan.org/pkg/hyperref
* https://www.tug.org/applications/hyperref/manual.html
* https://en.wikipedia.org/wiki/Comparison_of_TeX_editors * https://www.ubuntupit.com/best-latex-editor-top-33-reviewed-for-linux-nerds/
* https://www.ubuntupit.com/best-latex-editor-top-33-reviewed-for-linux-nerds/
* https://www.sharelatex.com
* [TeXMaker](http://www.xm1math.net/texmaker/) (Linux, iOS, Windows)
* Kile (GNU/Linux)
* [TeXStudio](https://www.texstudio.org/)
* overleaf.com
* WinEdit
* https://miktex.org/
* https://www.lyx.org/
* https://beebom.com/best-latex-editors/
* ...
