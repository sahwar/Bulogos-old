# A list of my favorite killer-apps for daily use (Software Tools of the Trade) (2019) #
> by https://github.com/sahwar 

## Under `Microsoft Windows 7 Ultimate/Pro SP1 (x64) / Windows 8.1 / Windows 10` ##
**Taskbar-pinned main apps:**
[ClassicShell](http://www.classicshell.net/) ([ClassicShell at GitHub](https://github.com/coddec/Classic-Shell), [Open-Shell (Classic Shell reborn, for Windows 10, too](https://github.com/Open-Shell)), `explorer.exe` (Windows Explorer file-manager), `taskmgr.exe` (Task Manager), `calc.exe` (Calculator), (`cmd.exe` / PowerShell), [Wox (speed-launcher)](https://github.com/Wox-launcher/Wox) (other [launchers](https://github.com/topics/launcher)), **[Notepad2-mod](https://github.com/XhmikosR/notepad2-mod/releases/)** / (**[Notepad3](https://github.com/rizonesoft/Notepad3/)** (_**[download](https://www.rizonesoft.com/downloads/notepad3/)**_) / **[Notepadqq](https://notepadqq.com/s/)** ([Notepadqq at GitHub](https://github.com/notepadqq/notepadqq)), **[notepad2e](https://github.com/ProgerXP/Notepad2e)**, [notepad2 mods by Code.Kliu.org](http://code.kliu.org/misc/notepad2/), [Notepad2, the original (by Florian Balmer)](http://www.flos-freeware.ch/notepad2.html)), **[Notepad++](https://notepad-plus-plus.org/) (_+add-ons, see below, e.g. DSpellCheck&TextFX Characters&MarkdownViewer++_)**, [BabelMap](http://www.babelstone.co.uk/Software/BabelMap.html) (Unicode character map), [(VoidTools) Search Everything](https://www.voidtools.com/) (file-searching-by-filename&regex engine for Windows), `AEDict.exe` ([AEDictionary](http://www.kamburov.net/aleksandar/projects/aedict/), an old but FAST Bulgarian<->English dictionary), [Megadict-BG](http://www.megadict-bg.com/) dictionary, [GoldenDict](http://goldendict.org/), [IDI Spellchecker&Dictionary](http://www.freeplace.info/ididictionary/bulgarian_spell_checker/) (v2.44/v4.??, buy the latest best version from [here](http://www.freeplace.info/ididictionary/bulgarian_spell_checker/)!), [paint.net](https://www.getpaint.net/), [GifCam](http://blog.bahraniapps.com/gifcam/) (bahraniapps.net), [ShareX](https://getsharex.com/), [nomacs](http://nomacs.org/) (image viewer), [Imagine](http://www.nyam.pe.kr/dev/imagine/) (image viewer), [NexusFont](http://www.xiles.net/) (font-manager), [Audacity](https://www.audacityteam.org/), [Mozilla Firefox](https://www.mozilla.org/bg/firefox/new/) (v56+, x64 as of 2018), [Google Chrome](https://www.google.com/chrome/), [MPC-HC](https://mpc-hc.org/) (Media Player Classic - Home Cinema), [VLC](https://www.videolan.org/), [Baka MPlayer](http://bakamplayer.u8sand.net/) ([mpv](https://mpv.io/)-based), [SMPlayer](https://www.smplayer.info/), Oracle VM [VirtualBox](https://www.virtualbox.org/) (OS virtualization), [PidginIM](http://pidgin.im/) (XMPP/Jabber IM client), [Skype](https://www.skype.com/en/get-skype/), [Viber](https://www.viber.com/), [Facebook Messenger](https://www.messenger.com), [EiskaltDC++](https://sourceforge.net/projects/eiskaltdcpp/) ([2](https://github.com/eiskaltdcpp/eiskaltdcpp))/[ApexDC](https://www.apexdc.net/)/[DC++](http://dcplusplus.sourceforge.net/index.html), [qBitTorrent](https://www.qbittorrent.org/), [youtube-dl.exe](http://youtube-dl.org/), [wget64.exe](https://eternallybored.org/misc/wget/), [curl64.exe](https://curl.haxx.se/download.html), [ffmpeg.exe](https://ffmpeg.org/) ([Windows builds](https://ffmpeg.zeranoe.com/builds/)) / [libav](https://www.libav.org/)... [7-ZIP](https://www.7-zip.org/) / [PeaZip](http://www.peazip.org/), [Krita](https://krita.org/en/), [MyPaint](http://mypaint.org/), [Medibang Paint Pro](https://medibangpaint.com/en/), [SpeedyPainter](http://speedypainter.altervista.org/), [JPixel](https://emad.itch.io/jpixel), `pdf2htmlEX`, `img2pdf`, [Rufus (ISO-writing-to-USB-flashstick tool)](https://github.com/pbatard/rufus) / [Etcher](https://github.com/balena-io/etcher), [FilePizza](https://github.com/kern/filepizza), [waifu2x](http://waifu2x.udp.jp/index.html), ..., [Shortcut (video-editor)](https://shotcut.org/), [Open Shot (video-editor)](https://www.openshot.org/) ([(translations over at LaunchPad.net)](https://translations.launchpad.net/openshot/2.0/+translations)), [Kdenlive](https://kdenlive.org/en/), [LibreCAD](https://librecad.org/), [Blender3D](https://www.blender.org/), [Greenfish Subtitle Player](http://greenfishsoftware.org/gfsp.php#apage) (FREEWARE, Windows-only), etc.

<!--
sudo apt-add-repository ppa:notepadqq-team/notepadqq
sudo apt-get update
sudo apt-get install notepadqq
-->
<!-- sudo snap install --classic notepadqq -->,

**Anti-virus:** Windows Firewall (Defender in Windows10) + [Avast Free](https://www.avast.com/free-antivirus-download), AVG Free is/was also OK.

**Full list of all apps I've ever used with Microsoft Windows XP/7/8.1/10:**
* (TO ADD from my [CCleaner](https://www.ccleaner.com/) `install.txt` files & other files)

## Under `(GNU/)Linux` ##
Too many to list here... for now at least, that is.

## Under `Apple macOS` ##
_I don't use macOS..._ In my opinion, it's only good for art/designer/webdev professionals... I only like some MacBook Pro's and the newer pressure-sensitive iPad's.

## Under `Google Android` ##
* (TO ADD from apps-list-generating apps)

## Under `Apple iOS` ##
_I don't use iOS..._
